import './App.css';
// import  './Components/PgSelectData'
import {PgSelectAll} from "./Components/PgSelectData";
import {DataForm} from "./Components/DataForm";
import {RedisSliceData} from "./Components/RedisSliceData";
import {KafkaConsumerData} from "./Components/KafkaConsumerData";
// import {RedisSliceData, DataForm, PgSelectAll} from "./Components/";
import React from "react";

function App() {
    console.log(process.env)
    return (
        <div className="App">
            <DataForm />
            <PgSelectAll />
            <RedisSliceData />
            <KafkaConsumerData />
        </div>
    );
}

export default App;
