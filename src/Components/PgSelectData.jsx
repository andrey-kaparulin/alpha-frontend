import React from "react";

const axios = require('axios');


class PgSelectAll extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            postgresPullData: null,
            isLoading: true
        }
    }

    componentDidMount = async () => {
        this.timer =  setInterval(() => {
            const apiHost = process.env.REACT_APP_API_HOST
            const apiPort = process.env.REACT_APP_API_PORT

            axios.get(`http://${apiHost}:${apiPort}/select_all_from_pg`).then(res => {
                this.setState({postgresPullData: res.data})
                this.setState({ isLoading: false });
            })
        }, 3000);
    }

    componentWillUnmount = async () => {
        clearInterval(this.timer);
    }

    render = () => {
        let { isLoading, postgresPullData } = this.state;
        if (isLoading) {
            return <div className="App">Loading...</div>;
        }
        return (
            <table>
                <thead>
                    <tr>
                      <th>PostreSQL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {postgresPullData.reverse().map((items, index) => {
                            return (
                                <div key={index}> {items[1]} </div>
                            );
                        })}
                    </tr>
                </tbody>
            </table>
        );
    }
}

export { PgSelectAll }
