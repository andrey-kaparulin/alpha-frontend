import React from "react";
const axios = require('axios');


class DataForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event) => {
        this.setState({value: event.target.value});
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const apiHost = process.env.REACT_APP_API_HOST
        const apiPort = process.env.REACT_APP_API_PORT
        const config = {headers: {"Content-Type": "application/json"}}

        // Send data to the PG
        await axios.post(`http://${apiHost}:${apiPort}/insert_to_pg?pg_val=${this.state.value}`, config)
        // Send data to the Redis list
        await axios.post(`http://${apiHost}:${apiPort}/push_data_to_list?redis_val=${this.state.value}`, config)
        // Send data to the Kafka topic
        await axios.post(`http://${apiHost}:${apiPort}/write_to_kafka_topic?topic_data=${this.state.value}`, config)

        this.setState({value: ""});
    }

    render = () => {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Value for record:
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Apply" />
            </form>
        );
    }
}

export { DataForm }
