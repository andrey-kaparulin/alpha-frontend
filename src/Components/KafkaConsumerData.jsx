import React from "react";
const axios = require('axios');


class KafkaConsumerData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            kafkaConsumerData: null,
            isLoading: true
        }
    }

    componentDidMount = async () => {
        this.timer =  setInterval(() => {
            const apiHost = process.env.REACT_APP_API_HOST
            const apiPort = process.env.REACT_APP_API_PORT

            axios.get(`http://${apiHost}:${apiPort}/get_topic_data`).then(res => {
                this.setState({kafkaConsumerData: res.data})
                this.setState({ isLoading: false });
            })
        }, 3000);
    }

    componentWillUnmount = async () => {
        clearInterval(this.timer);
    }

    render = () => {
        let { isLoading, kafkaConsumerData } = this.state;
        if (isLoading) {
            return <div className="App">Loading...</div>;
        }
        return (
            <table>
                <thead>
                    <tr>
                      <th>Kafka</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {kafkaConsumerData.reverse().map((item, index) => {
                            return (
                                <div key={index}> {item} </div>
                            );
                        })}
                    </tr>
                </tbody>
            </table>
        );
    }
}

export { KafkaConsumerData }
