import React from "react";
const axios = require('axios');


class RedisSliceData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redisSliceData: null,
            isLoading: true
        }
    }

    componentDidMount = async () => {
        this.timer =  setInterval(() => {
            const apiHost = process.env.REACT_APP_API_HOST
            const apiPort = process.env.REACT_APP_API_PORT

            axios.get(`http://${apiHost}:${apiPort}/get_full_list_slice`).then(res => {
                this.setState({redisSliceData: res.data})
                this.setState({ isLoading: false });
            })
        }, 3000);
    }

    componentWillUnmount = async () => {
        clearInterval(this.timer);
    }

    render = () => {
        let { isLoading, redisSliceData } = this.state;
        if (isLoading) {
            return <div className="App">Loading...</div>;
        }
        return (
            <table>
                <thead>
                    <tr>
                      <th>Redis</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {redisSliceData.map((item, index) => {
                            return (
                                <div key={index}> {item} </div>
                            );
                        })}
                    </tr>
                </tbody>
            </table>
        );
    }
}

export { RedisSliceData }
