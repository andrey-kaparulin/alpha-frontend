# build environment
FROM node:lts-alpine3.16 as build
# since env vars should be passed on "npm build stage"
# and there is no way pass it for k8s dirrectly, we use ARG
# pls use make file, in root dir
ARG REACT_APP_API_HOST
ARG REACT_APP_API_PORT
ENV REACT_APP_API_HOST=$REACT_APP_API_HOST
ENV REACT_APP_API_PORT=$REACT_APP_API_PORT

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm ci --silent
COPY . ./
RUN npm run build

# production environment
FROM nginx:1.23-alpine@sha256:96a447b9474aff02d4e3b642f5fdb10ff7ff25d409e8132e536c71f494f59617
COPY --from=build /app/build /usr/share/nginx/html
